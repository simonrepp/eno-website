import path from 'path';

export default {
    entry: {
        playground: {
            filename: 'playground/bundle.js',
            import: './src/playground.js'
        }
    },
    mode: 'production',
    // optimization: { minimize: false },
    output: {
        path: path.resolve('public')
    },
    stats: 'summary'
};