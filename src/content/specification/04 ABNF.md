# ABNF

This is an attempt to write as much of eno's specification down as ABNF. Note
that it is currently not complete nor up to date with the final specification,
and that by design it is not possible to cover all aspects of the specification
in that format (or at least not *practically* possible).

```abnf
;; *************************************
;; *                                   *
;; *        WORK IN PROGRESS           *
;; *                                   *
;; *************************************


document = instruction *( 1*newline instruction )


;; Whitespace

ignored-whitespace = whitespace
retained-whitespace = whitespace

whitespace = 1*( SP / HTAB )

SP   = U+0020  ; " " (Space)
HTAB = U+0009  ; "  " (Horizontal tab)


;; Newline

newline = LF / CR LF

LF = U+000A  ; "\n" (Line feed)
CR = U+000D  ; "\r" (Carriage return)


;; Element

instruction = trimmed-instruction / embed-text

trimmed-instruction = [ignored-whitespace] instruction-body [ignored-whitespace]

instruction-body =  attribute
instruction-body =/ comment
instruction-body =/ embed
instruction-body =/ field
instruction-body =/ item


;; Attribute

attribute = escapable-key [ignored-whitespace]
              attribute-operator [ ( [ignored-whitespace] value ) ]

attribute-operator = EQUALS-SIGN

EQUALS-SIGN = U+003D  ; "="


;; Comment

comment = comment-operator [ ( [ignored-whitespace] value ) ]

comment-operator = GREATER-THAN-SIGN

GREATER-THAN-SIGN = U+003E  ; ">"


;; Embed

embed = embed-operator [ignored-whitespace] key

embed-operator = 2*DASH

embed-text = [retained-whitespace] value


;; Field

field = escapable-key [ignored-whitespace] field-operator [ ( [ignored-whitespace] value ) ]

field-operator = COLON

BACKTICK = U+0060  ; "`"
COLON = U+003A     ; ":"


;; Item

item = item-operator [ ( [ignored-whitespace] value ) ]

item-operator = DASH

DASH = U+002D  ; "-"


;; Key

escapable-key = escaped-key / key

escaped-key = 1*BACKTICK [ignored-whitespace] required-token [ignored-whitespace] 1*BACKTICK  ; Note: The number of backticks at the start and end must be the same

key = required-token


;; Section

section = section-operator [ignored-whitespace] key

section-operator = 1*HASH

HASH = U+0023


;; Token

optional-token = *non-eol
required-token = 1*non-eol

non-eol =  %x09
non-eol =/ %x20-10FFFF


;; Value

value = optional-token
```