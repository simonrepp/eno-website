# Parsing Errors

The eno language is simple enough for us to distinctly identify all grammatical
error cases that can occur during parsing in simple, concrete terms, without
needing to resort to generic messages such as "Invalid line".

Although these distinct error cases can be conceptualized and worded in
different ways - and it is indeed left to parser authors to make the final
choice about this - the language specification provides a template of
error cases/labels that can be used as a reference:

`attribute_outside_field`

```eno
attribute = value
```

`attribute_without_key`

```eno
= value
```

`embed_without_key`

```eno
--
```

`escape_without_key`

```eno
` `
```

`field_without_key`

```eno
: value
```

`invalid_after_escape`

```eno
`key` value
```

`item_outside_field`

```eno
- item
```

`mixed_field_content`

```eno
field: value
attribute = value

field:
- item
attribute = value

field: value
- item

field:
attribute = value
- item
```

`section_level_skip`

```eno
# section
### section
```

`section_without_key`

```eno
#
```

`unterminated_embed`

```eno
-- embed
...
```

`unterminated_escaped_key`

```eno
`key
```