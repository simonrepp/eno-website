# Glossary

All terminology used for eno, listed alphabetically and in its singular form.

## Attribute

```eno
my_attribute = my value
my_attribute =
```

An `Attribute` can have a `Value` or be empty.

## Attribute Operator

The `=` character when it appears after a `Key`, optionally followed by a `Value`.

## Comment

```eno
> my comment
```

A `Comment` is indicated by the `Comment Operator` and followed by arbitrary text.

## Comment Operator

The `>` character (U+003E), when it occurs as the first non-whitespace character on a line, thereby starting a `Comment`.

## Document

The top level structure that contains everything in an `.eno` file.
Conceptually equivalent to a `Section`, but without a key and implicitly always there.

## Element

All nodes in a `Document` that can later be traversed by an application, that is a `Attribute`, `Embed`, `Field`, `Flag`, `Item` or `Section`.

## Escaped Key

```eno
`my_escaped_key`:
```

A `Key` contained between `Escape Operators`.

## Escape Operator

Any number of connected `` ` `` characters (U+0060) when they appear as the first non-whitespace characters in a line, indicating an `Escaped Key`. It must be followed by exactly the same `Escape Operator` later in the same line, terminating the `Escaped Key`.

## Field

```eno
my_key: my value

my_key:
my_attribute_a = my value
my_attribute_b = my value

my_key:
- my value
- my value
```

A `Field` has a `Key`. It can contain either a `Value`, `Attributes`, or `Items`.

## Field Operator

The `:` character (U+003A) when it appears after a `Key`, optionally followed by a `Value`.

## Flag

```eno
my_key
```
A `Flag` element is a distinct element type only containing a `Key`.

## Item

```eno
- my value
-
```

An `Item` can have a `Value` or be empty.

## Item Operator

A `-` character (U+002D), when it appears as the first non-whitespace character in a line, indicating an `Item`.

## Embed

```eno
-- my_key
my multiline value ...
-- my_key

-- my_key
a
-- my_key

-- my_key
-- my_key
```

An `Embed` has a `Key` and a `Value`. The `Key` appears in the beginning and ending line. It's genuinely empty if there is no line between the beginning and ending line, whereas a single line with 0 characters between the beginning and ending line creates an empty string value.

## Embed Operator

Two or more consecutive `-` characters (U+002D) when they appear as the first
non-whitespace characters on a line and followed by a `Key`, indicating the
begin or end of an `Embed`. The number of hyphens that make up the
`Embed Operator` in the ending line of an `Embed` has to
match the number in the beginning line, just as the `Key` needs to be repeated
exactly, otherwise the whole line is treated as simple text inside the
`Embed`.

## Key

```eno
my_key:
```

The thing that is also referred to as *property*, *identifier*, *variable name*
etc. in other languages is always designated as `Key` in eno. A `Key` can be
given to an `Attribute`, `Embed`, `Field`, `Flag` and `Section`. Note that
conceptually the `Key` of an `Item` is the `Key` of the `Field` its in, as a
`Value` with no association whatsoever makes little sense in a processable data
context.

## Operator

Any of `>`, `:`, `=`, `-`, `--`, `#`, `|`, `\`, `` ` `` when they appear outside of a `Comment`, `Key` or `Value`.

## Section

```eno
# my_key
## my_key
```

A `Section` has a `Key` and a `Depth` or `Level`, determined by the number of
hashes that make up the `Section Operator`. It contains `Elements`. The first
`Section` to appear in a document must have a `Depth` of `1`, that is, the
`Section Operator` being a single `#`. Any following section can have a `Depth`
that is at most the `Depth` of the immediately preceding `Section` plus one, so
`# section` may be followed by `# section` or `## section`,  but not by `###
section`.

## Section Operator

Any number of connected `#` characters (U+0023) when they appear as the first non-whitespace characters on a line, indicating a `Section`.

## Value

```eno
my_key: my_value
```

A `Value` might appear in an `Attribute`, `Embed`, `Field` or `Item`.
`Values` can only include newlines and leading or trailing whitespace when they come from an `Embed`.