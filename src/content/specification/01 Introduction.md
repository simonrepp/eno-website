# Specification

If you just want to use eno, see the [guide](/guide/). If you want to
implement an eno library or otherwise need deeper insight on the language,
this is the right resource, read on.

## Encoding

Eno documents **must** be encoded as UTF-8.

## Filename Extension

When saved to disk, eno documents **should** use the `.eno` extension.

## MIME Type

There are plans to officialy apply to the IANA to get `text/eno` registered as an offical
MIME type for eno documents. If you would like to follow that process or bring in expertise
and support you are very welcome to do so in the respective [issue on github](https://github.com/eno-lang/eno/issues/1).

## Ordering

Eno contains only ordered data structures. The order of elements as authored in
a document is always retained by parsers.

## Types

In eno there is no type differentiation on the language level, there are only
textual representations ("strings"), which are always referred to as *values*.