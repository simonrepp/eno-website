# Escaping

Let's assume someone wrote an eno document describing which clothing to wear
at different temperatures:

```eno
30 degrees celsius: Swimming trunks
10 degrees celsius: Warm pullover and windjacket
`-10 degrees celsius`: A very warm coat
```

You'll notice that something is different in the third line: The `-10 degrees celsius` has been put between two backticks ( `` ` `` ).

Here's why:  
`-10 degrees celsius` starts with a `-`, and in eno every line
starting with a `-` is interpreted as an *item* as we learned before,
therefore we need some way to say that we really mean "minus 10 degrees
celsius" and not "10 degrees celsius" as an item.

Here are some more examples of escaping in eno:

```eno
`https://example.org/`: An example website
```
Every field in eno follows the pattern `key: value`, but in this case our key `https://example.org/` contains a `:` already, therefore we need to escape it.

```eno
`` `hypothetical` ``: The word "hypothetical" inside backticks
```
You might have wondered how an escape can be escaped, this is achieved by using two backticks around the key that is itself wrapped in (single) backticks, and leaving spaces so it does not look like three backticks. (these spaces on the outside are not included in the key though)

Escaping can also be required for an embed, namely if a line *inside* the
embed looks exactly like the opening/ending line of the embed itself. It can
be escaped by adding any number of additional dashes to the `--` operator
in *both* the opening *and* ending line:

```eno
--- My Field
-- My Field
--- My Field
```

Lastly note that escaping only is supported/required for the *key* of a field,
attribute, flag and for embeds - simply because anything else does not ever
need to be escaped! The keys of sections and all values in eno always occur
as the last (or only) token in a line, thereby their content can be used
verbatim and unambiguously, no matter what they contain.
