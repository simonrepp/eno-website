# Whitespace

In general, all whitespace before, between and after any token is stripped away.
The two following examples produce exactly the same output:

```eno
#section
field:a field's value
field:
-item
```

... is exactly equal to ...

```eno
  #       section
    field   :   a field's value
    field:
        -    item
```

The only exception to this is the value of an embed. All whitespace - leading,
trailing and empty lines - inside an embed is always kept.

This embed holds the value "The Value":

```eno
-- my_embed
The Value
-- my_embed
```

This embed however produces the result "\n  The Value\n"

```eno
-- my_embed

  The Value

-- my_embed
```

## Expressing keys/values with leading/trailing whitespace

In the rare case this is needed (outside of embed values, which support this),
one can use any custom syntax to mark the begin and end of a key/value,
e.g. using quotes as other languages do it for strings, and then removing these
extra characters in processing:

```eno
# " section "

" key ": " value "
```

Strong emphasis here that the above is not a language feature of eno but just
an arbitrary example of how whitespace can be expressed - you might as well
use single quotes or pipe characters or anything else, to eno it's just
characters in a key or value with no special significance!

Note that using eno's escaping (which *is* a native language feature) does not
allow you to express leading/trailing or pure whitespace in keys, the outer spacing
in an escape sequence allows to express an ambiguous case of *escaping an
escape sequence* but is always trimmed away:

```eno
`` my_name ``: my value 
`` `my_name` ``: my value 
```

Here the resulting key is `'my_name'` in the first and ``'`my_name`'`` in the second example.