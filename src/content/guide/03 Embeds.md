# Embeds

If we want to express content with multiple lines, we do it like this:

```eno
-- my_poem
Roses are red
Violets are blue
The dashes and the key in the start and end line need to match
-- my_poem
```

Unlike everywhere else in an eno document, the inside/value of an embed
retains all whitespace (tabs, spaces, linebreaks, etc.) verbatim, so this
would for instance be a way to express two empty lines:

```eno
-- text


-- text
```

Note that leaving no extra line at all between the opening and ending lines produces
a field with no value at all (`null` in developer terms):

```eno
-- My Field
-- My Field
```