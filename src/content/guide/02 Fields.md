# Fields

At its core eno lets us write down values, the simplest form being:

```eno
Current Temperature: Quite pleasant
```

With this we specify a pair of a key and a value, the key being "Current temperature" and the value being "Quite pleasant".

Some other examples:

```eno
Last Name: Schneider
Website: example.com
Profession: Freelance graphics artist
```

If we don't want to specify a value, we just omit it. (Note for developers: the value then is `null`, not an empty string)

```eno
More information:
```

## Attributes

Instead of a value, a field can also contain *attributes*. An attribute looks and
works almost like a field, except that it is contained within a field and
differentiated from it by the operator, an equals sign (`=`). A key can appear
multiple times among the attributes in a field, but applications may choose to
accept only unique keys when that behavior fits the usecase better.

```eno
color ratings:
green = 3
red = 4
blue = 1
```

As with fields, the value can be omitted (producing a `null` value).

```eno
image:
src =
```

## Items

As a third alternative, a field can also contain *items*, which is to say, a list of multiple values.

```eno
Shopping list:
- Bread
- Rice
- Tomatos
```

Items can also have empty (`null`) values (this can e.g. be used for grouping/spacing usecases):

```eno
Sort Order Select:
- Default
- 
- Title
- Year
```