# Overview

This guide is written for users of all technical levels and best read in the
order it is presented. Software professionals might prefer to go straight to the
[cheatsheet](cheatsheet) and then jump back to specific sections to
learn about details.

## Creating and editing eno files

Eno files are *plaintext* files with the file extension `.eno`. It is strongly
recommended to **avoid using a rich text editor** (such as Word or TextEdit) to create
or edit eno files, as these programs tend to silently turn your *plaintext* files into
*rich text* files, which renders them useless for further processing. Here are some
editors that will correctly handle eno files as plaintext only:

- [Pulsar](https://pulsar-edit.dev/) (All platforms)
- [Notepad++](https://notepad-plus-plus.org/) (Windows)
- [TextMate](https://macromates.com/) (macOS)
- [gedit](https://gedit-technology.github.io/apps/gedit/) (Linux)

This guide will further explain all the things you can put into an eno file.

# Comments

Lines starting with `>` are considered comments and may contain free form text.
They are there for us to make notes and leave hints for others reading the document.

```eno
> This is a comment
```
