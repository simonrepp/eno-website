# Sections

Sections let us semantically group together elements:

```eno
# Contact data

Telephone: 02-2285-8855
Email: contact@example.org
```

This assigns all elements that follow after the begin of the section "Contact data" to that section. It is also possible to create any number of sub-sub-(and so on)-sections:

```eno
# Contact data

## Telephone numbers

### Mary-Ann

Landline: n/a
Mobile: 089 011 0819

### Emre

Landline: n/a
Mobile: (0)176-040690 78
```

An example that combines various other element types with sections:

```eno
# Profile

Name: Estelle Smith
Hair color: green/gray
Age:
Activities:
- Drawing mangas
- Climate activism

-- Quote
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
-- Quote

## Misc

Friends:
Jane Doe = A close friend of hers
Jean Doe = Rather a false friend
John Dear = A casual acquaintance
```