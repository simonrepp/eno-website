# Flags

A key just by itself indicates a *Flag*. Flags have two primary usecases - the
first one is as a global flag/switch/boolean:

```eno
enable_debugging!
LOG_LEVEL_INFO
utf_8
```

The second main usecase is as a spacer or key-only element in an ordered
collection, e.g. inside some custom markup DSL:

```eno
header

heading: New stable release available

padding

-- text
Today we've released our new stable version (codenamed "Leverage")
which will be available on all release channels by midday, ...
-- text

footer
```