# Eno <span class="light">· A Data Language For Everyone</span>

```eno
author: Jane Doe
email: jane@example.org

-- my_content
Multi-line embedded content (e.g. markdown) here ...
-- my_content

states:
active = #fff
hover = #b6b6b6

# cities

Den Haag: 52.069961, 4.302315
Málaga: 36.721447, -4.421291
서울특별시: 37.566984, 126.977041
```

Eno is a data language for all people, not just developers. Its simple syntax
and versatile nature welcomes a wide audience, both in regards to cultural
background as well as technical ability. Unlike most data languages, Eno is
entirely type-agnostic, making it one of the easiest data languages to grasp and
author content in.

- [Guide](overview.html) - The entire language explained in just a few minutes
- [Playground](playground/) - Example documents you can interactively modify and study
- [Specification](introduction.html) - Encoding, extension, MIME type and implementation details


# Plugins <span class="light">· Syntax Highlighting</span>

- [Ace](https://codeberg.org/simonrepp/ace-eno/) - Mode and highlight rules, ready-to-use custom build
- [Prism](https://codeberg.org/simonrepp/prism-eno/) - Language grammar definition, custom color schemes
- [Pulsar](https://web.pulsar-edit.dev/packages/language-eno) - Syntax highlighting, embedded language support
- [Sublime Text](https://packagecontrol.io/packages/eno) - Syntax highlighting, default color theme customizations
- [Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=eno-lang.vscode-eno) - Syntax highlighting

# Libraries <span class="light">· Parsers</span>

- [C/C++](https://codeberg.org/simonrepp/libeno/) - libeno
- [Haskell](https://github.com/eno-lang/tree-sitter-eno/) - tree-sitter-eno parser (via bindings)
- [Java](https://github.com/Nzen/Enohar/) - Enohar library by Nicholas Prado (incomplete)
- [JavaScript](https://codeberg.org/simonrepp/enolib-js) - enolib library, current reference parser
- [PHP](https://codeberg.org/simonrepp/enofn-php/) - enofn parser
- [Python](https://codeberg.org/simonrepp/enolib-py) - enolib library
- [Ruby](https://codeberg.org/simonrepp/enolib-rb) - enolib library
- [Rust](https://codeberg.org/simonrepp/enolib-rs/) - enolib library
- [Tcl](https://github.com/bef/enotcl/) - enotcl parser by BeF (incomplete)
- [WebAssembly](https://github.com/eno-lang/tree-sitter-eno/) - tree-sitter-eno parser (via bindings)

# Libraries Addendum <span class="light">· Types And More</span>

- [Enotype](https://github.com/eno-lang/enotype/) - Common types for use with enolib in multiple languages
- [Enolib Benchmarks](https://codeberg.org/simonrepp/benchmarks/) - Multi-language comparison with toml, yaml parsers

# History <span class="light">· Past</span>

Eno emerged from work on a large publishing project in 2018. It was developed by
[Simon Repp](https://simonrepp.com/), and still is to this day. Many others
contributed their cultural, technical and personal insight, reported bugs,
submitted fixes and set out to develop applications with Eno - I'd like to
express my gratitude and thank you for your support.

# Present <span class="light">· And Beyond</span>

In May 2022 a new [minimal php
parser](https://codeberg.org/simonrepp/enofn-php/) was released. A [rust
parser](https://codeberg.org/simonrepp/enolib-rs) has recently spun off the
[faircamp](https://codeberg.org/simonrepp/faircamp) project and seen much
progress. A new, reference [C-based
parser](https://codeberg.org/simonrepp/libeno/) has been in the making for a
while. The long-going effort to wrap up the language specification is
accompanying these developments, drawing ever smaller circles around the target.

For occasional updates follow
[@freebliss@post.lurk.org](https://post.lurk.org/@freebliss) on the fediverse.
For technical questions feel free to open issues in the repositories. You can
also reach out via mail to <simon@fdpl.io>.
