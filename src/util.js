const ATTR_ESCAPE_MAP = {
    '"': '&quot;',
    "'": '&#39;'
};

export const attrEscape = string => string.replace(/["']/g, c => ATTR_ESCAPE_MAP[c]);

const HTML_ESCAPE_MAP = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;'
};

export const htmlEscape = string => string.replace(/[&<>"'\/]/g, c => HTML_ESCAPE_MAP[c]);

const ATTR_UNESCAPE_MAP = {
    '&quot;': '"',
    '&#39;': "'"
};

export const attrUnescape = string => string.replace(/&quot;|&#39;/g, c => ATTR_UNESCAPE_MAP[c]);