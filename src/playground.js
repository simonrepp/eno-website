import { parse, HtmlReporter, ParseError } from 'enolib';

import { attrUnescape, htmlEscape } from './util.js';

const editor = document.querySelector('#editor');
const error = document.querySelector('.error');
const inspector = document.querySelector('.inspector');
const selectDemo = document.querySelector('.demo');

import { createNode } from './node.js';

let documentNode = null;

const refresh = () => {
    const input = editor.value;

    try {
        const doc = parse(input, { reporter: HtmlReporter });

        if (documentNode) {
            documentNode.update(doc);
        } else {
            documentNode = createNode(doc, 'document');
            inspector.append(documentNode.element);
        }

        error.classList.remove('active');
        inspector.classList.add('active');
    } catch(err) {
        if (err instanceof ParseError) {
            error.innerHTML = err;

            inspector.classList.remove('active');
            error.classList.add('active');
        } else {
            throw err;
        }
    }
};

// TODO: Solve data attribute encoding by embedding data in a script tag

selectDemo.addEventListener('change', () => {
    const demoOption = selectDemo.selectedOptions[0];
    editor.value = attrUnescape(demoOption.dataset.eno);

    refresh();
});

editor.addEventListener('input', refresh);

refresh();
