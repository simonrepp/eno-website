import markdownIt from 'markdown-it';
import markdownItPrism from 'markdown-it-prism';

const markdownItHtml = markdownIt({ html: true });
markdownItHtml.use(markdownItPrism);
export const markdown = value => markdownItHtml.render(value);

import Prism from 'prismjs';
import './prism-eno.js';