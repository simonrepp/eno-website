const formatValue = value => {
    if(value === null)
        return 'null';

    value = value.replace('\n', '\\n');

    if(value.length > 32) {
        value = value.substr(0, 31) + '…';
    }

    return value;
};

export function createNode(element, type) {
    let elementMemo = element;

    const node = document.createElement('div');

    const body = document.createElement('div');
    node.append(body);

    const children = document.createElement('div');
    children.classList.add('indented');
    node.append(children);

    let folded = false;
    let foldable = false;
    let prevComment = null;
    let prevElements = [];
    let prevFolded = folded;
    let prevKey = null;

    const img = document.createElement('img');
    img.classList.add('node');

    const a = document.createElement('a');
    a.append(img);
    body.append(a);

    const keySpan = document.createElement('span');

    keySpan.classList.add('key');

    if (type === 'document') {
        keySpan.innerHTML = 'Document';
    }

    body.append(keySpan);

    const valueSpan = document.createElement('span');
    valueSpan.classList.add('value');
    body.append(valueSpan);

    if (type !== 'document') {
        const typeMap = {
            'attribute': '(Attribute)',
            'embed': '(Embed)',
            'flag': '(Flag)',
            'field': '(Field)',
            'item': '(Item)',
            'section': '(Section)'
        };

        const typeSpan = document.createElement('span');
        typeSpan.classList.add('element_type');
        typeSpan.innerHTML = typeMap[type];
        body.append(typeSpan);
    }

    const commentSpan = document.createElement('span');
    commentSpan.classList.add('comment');
    body.append(commentSpan);

    let update;

    const updateShared = element => {
        elementMemo = element;

        const comment = element.optionalStringComment();
        if (comment !== prevComment) {
            commentSpan.innerHTML = comment ? formatValue(comment) : '';
            prevComment = comment;
        }

        if (type === 'item') {
            valueSpan.innerHTML = element.optionalStringValue() || 'null';
        } else {
            const key = element.stringKey();
            if (key && key !== prevKey) {
                keySpan.innerHTML = key;
                prevKey = key;
            }

            if (element.isField() && (element.hasAttributes() || element.hasItems() || !element.hasValue())) {
                valueSpan.innerHTML = '';
            } else if (element.isEmbed() || element.hasValue()) {
                const value = element.optionalStringValue();

                if (value) {
                    valueSpan.innerHTML = `→ ${formatValue(value)}`;
                } else {
                    valueSpan.innerHTML = '→  null';
                }
            }
        }

        a.classList.toggle('no-fold', !foldable);

        if (foldable) {
            img.src = folded ? '../images/unfold.svg' : '../images/fold.svg';
        } else {
            img.src = '../images/unfoldable.svg';
        }
    };

    if (type === 'document' || type === 'section') {
        update = element => {
            const elements = folded ? [] : element.elements();
            elements.forEach((element, index) => {
                if (element.isEmbed()) {
                    if (prevElements.length > index) {
                        if (prevElements[index].type === 'embed') {
                            prevElements[index].update(element);
                        } else {
                            const embedNode = createNode(element, 'embed');
                            children.replaceChild(embedNode.element, prevElements[index].element);
                            prevElements[index] = embedNode;
                        }
                    } else {
                        prevElements.push(createNode(element, 'embed'))
                        children.append(prevElements[index].element);
                    }
                }
                
                if (element.isFlag()) {
                    if (prevElements.length > index) {
                        if (prevElements[index].type === 'flag') {
                            prevElements[index].update(element);
                        } else {
                            const flagNode = createNode(element, 'flag');
                            children.replaceChild(flagNode.element, prevElements[index].element);
                            prevElements[index] = flagNode;
                        }
                    } else {
                        prevElements.push(createNode(element, 'flag'))
                        children.append(prevElements[index].element);
                    }
                }
                
                if (element.isField()) {
                    if (prevElements.length > index) {
                        if (prevElements[index].type === 'field') {
                            prevElements[index].update(element);
                        } else {
                            const fieldNode = createNode(element, 'field');
                            children.replaceChild(fieldNode.element, prevElements[index].element);
                            prevElements[index] = fieldNode;
                        }
                    } else {
                        prevElements.push(createNode(element, 'field'))
                        children.append(prevElements[index].element);
                    }
                }

                if(element.isSection()) {
                    if (prevElements.length > index) {
                        if (prevElements[index].type === 'section') {
                            prevElements[index].update(element);
                        } else {
                            const sectionNode = createNode(element, 'section');
                            children.replaceChild(sectionNode.element, prevElements[index].element);

                            // prevElements[index].element.remove(); --- probably garbage collected anyway?
                            // TODO: Check what .remove() actually does (e.g. at mdn)

                            prevElements[index] = sectionNode;
                        }
                    } else {
                        prevElements.push(createNode(element, 'section'))
                        children.append(prevElements[index].element);
                    }
                }
            });

            for (let index = elements.length; index < prevElements.length; index++) {
                prevElements[index].element.remove();
            }

            while (prevElements.length > elements.length) {
                const lastPrevElement = prevElements.pop();
                lastPrevElement.element.remove();
            }

            foldable = element.elements().length > 0;

            updateShared(element);
        };
    } else if (type === 'field') {
        
        update = element => {
            let childrenType, fieldChildren;
            
            if (element.hasAttributes()) {
                childrenType = 'attribute';
                fieldChildren = element.attributes();
                foldable = true;
            } else if (element.hasItems()) {
                childrenType = 'item';
                fieldChildren = element.items();
                foldable = true;
            } else {
                fieldChildren = [];
                foldable = false;
            }
            
            const fieldChildrenFolded = folded ? [] : fieldChildren;
            
            fieldChildrenFolded.forEach((child, index) => {
                if (prevElements.length > index) {
                    if (prevElements[index].type === childrenType) {
                        prevElements[index].update(child);
                    } else {
                        const retypedNode = createNode(child, childrenType);
                        children.replaceChild(retypedNode.element, prevElements[index].element);

                        // prevElements[index].element.remove(); --- probably garbage collected anyway?
                        // TODO: Check what .remove() actually does (e.g. at mdn)

                        prevElements[index] = retypedNode;
                    }
                } else {
                    prevElements.push(createNode(child, childrenType))
                    children.append(prevElements[index].element);
                }
            });
            
            for (let index = fieldChildrenFolded.length; index < prevElements.length; index++) {
                prevElements[index].element.remove();
            }
            
            while (prevElements.length > fieldChildrenFolded.length) {
                const lastPrevElement = prevElements.pop();
                lastPrevElement.element.remove();
            }
            
            updateShared(element);
        };
    } else {
        update = element => updateShared(element);
    }

    a.addEventListener('click', () => {
        folded = !folded;
        update(elementMemo);
    });

    update(element);

    return {
        element: node,
        type,
        update
    };
}
