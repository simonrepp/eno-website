import fs from 'fs';
import { markdown } from './loaders.js';
import { parse, TerminalReporter } from 'enolib';
import path from 'path';
import slugify from 'speakingurl';

export function sourcePages(directory) {
    const directoryFull = path.resolve('src/content', directory);
    const files = fs.readdirSync(directoryFull);
    
    files.sort();

    const pages = [];

    for (const file of files) {
        const title = file.substring(3, file.length - 3);
        const contentMd = fs.readFileSync(path.join(directoryFull, file), 'utf-8');
        const content = markdown(contentMd);

        pages.push({
            content,
            slug: slugify(title),
            title
        });
    }
    
    return pages;
};

export function sourcePlayground() {
    const input = fs.readFileSync(path.resolve('src/content/playground.eno'), 'utf-8');
    const document = parse(input, { reporter: TerminalReporter, source: 'src/content/playground.eno' });
    
    const demos = document.sections().map(groupSection => ({
        examples: groupSection.sections().map(exampleSection => ({
            eno: exampleSection.embed('eno').requiredStringValue(),
            title: exampleSection.stringKey()
        })),
        title: groupSection.stringKey()
    }));
    
    document.assertAllTouched();
    
    return demos;
}