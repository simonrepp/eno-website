import fs from 'fs';
import fsExtra from 'fs-extra';
import path from 'path';

import { playground } from './playground_generator.js';
import { markdown } from './loaders.js';
import { sourcePages, sourcePlayground } from './source.js';

const data = { playground: sourcePlayground() };

data.assetHash = Math.floor(Math.random() * 1000000);

fsExtra.emptyDirSync(path.resolve('public'));
fsExtra.copySync(path.resolve('src/static/'), path.resolve('public/'));

const file = path.resolve('src/content/home.md');
const input = fs.readFileSync(file, 'utf-8');
const homeHtml = markdown(input);

function layout (pages, activePage, content) {
    return `
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="${activePage.title}">
        <title>${activePage.title}</title>
        <link rel="stylesheet" href="styles.css?${pages.assetHash}">
    </head>
    <body>
        <div class="docs">
            <nav>
                <a ${activePage == pages.home ? 'class="active"' : ''} href="index.html">
                    Introduction
                </a>

                <span>Guide</span>
                ${pages.guide.map(page => `
                    <a ${page == activePage ? 'class="active"' : ''} href="${page.slug}.html">
                        ${page.title}
                    </a>
                `).join('')}

                <span>Specification</span>
                ${pages.specification.map(page => `
                    <a ${page == activePage ? 'class="active"' : ''} href="${page.slug}.html">
                        ${page.title}
                    </a>
                `).join('')}
            </nav>
            <main>
                <div class="main_boundary">
                    ${content}
                </div>
            </main>
        </div>
    </body>
</html>
    `.trim();
}

const pages = {
	assetHash: Math.floor(Math.random() * 1000000),
	guide: sourcePages('guide'),
	home: {
		content: markdown(homeHtml),
		slug: 'index',
		title: 'Introduction'
	},
	specification: sourcePages('specification')
};

function renderPage (pages, activePage, nextPage) {
	let body;
	if (nextPage) {
		body = `
			${activePage.content}

			<div class="next_page">
				Next page: <a href="${nextPage.slug}.html">${nextPage.title}</a>
			</div>
		`.trim();
	} else {
		body = activePage.content;
	}

	const html = layout(pages, activePage, body);

    const fileAbsolute = path.resolve('public', `${activePage.slug}.html`);
    fs.writeFileSync(fileAbsolute, html);
}

renderPage(pages, pages.home, pages.guide[0]);

for (let index = 0; index < pages.guide.length; index++) {
	const activePage = pages.guide[index];
	const nextPage = index < pages.guide.length - 1 ? pages.guide[index + 1] : pages.specification[0]; 
    renderPage(pages, activePage, nextPage);
}

for (let index = 0; index < pages.specification.length; index++) {
	const activePage = pages.specification[index];
	const nextPage = index < pages.specification.length - 1 ? pages.specification[index + 1] : null; 
    renderPage(pages, activePage, nextPage);
}

playground(data);
