const elementRest = {
    _field: {
        pattern: /:[^\n]*/,
        inside: {
            'operator': /^:/,
            _value: /.*/
        }
    },
    _attribute: {
        pattern: /=[^\n]*/,
        inside: {
            'operator': /^=/,
            _value: /.*/
        }
    },
    'invalid': /\S+/
};

Prism.languages.eno = {
    // > ...
    'comment': {
        pattern: /(^\s*)>[^\n]*$/m,
        lookbehind: true
    },
    // -- ...
    _embed: {
        alias: '_value',
        pattern: /(^|\n)[^\S\n]*(-{2,})(?!-)[^\S\n]*(\S[^\n]*?)[^\S\n]*\n(?:[\s\S]*?\n)?[^\S\n]*\2(?!-)[^\S\n]*\3[^\S\n]*(?:\n|$)/,
        inside: {
            _begin: {
                pattern: /^[^\S\n]*-{2,}(?!-)[^\n]+/,
                inside: {
                    'operator': /^[^\S\n]*(-{2,})(?=[^\S\n]*\S)/,
                    'tag name': /.*/
                }
            },
            _end: {
                pattern: /\n[^\S\n]*-{2,}(?!-)[^\n]*\n?$/,
                inside: {
                    'operator': /^\n[^\S\n]*(-{2,})/,
                    'tag name': /.*/
                }
            }
        },
        lookbehind: true
    },
    // # ...
    '_section': {
        pattern: /(^\s*)#[^\n]*$/m,
        lookbehind: true,
        inside: {
            'keyword section-operator': /^#{1,}(?!#)/,
            _escaped_key: {
                pattern: /(`+)(?!`)(?:(?!\1).)*\1(?=\s*$)/,
                inside: {
                    'operator': /(^`+|`+$)/,
                    'tag name': /.*/
                }
            },
            'tag name': /[^\s`].*/
        }
    },
    // ` ...
    _escaped_element: {
        pattern: /(^\s*)`[^\n]*$/m,
        lookbehind: true,
        inside: {
            _escaped_attribute_key: {
                pattern: /^(`+)(?!`)(?:(?!\1).)*\1(?=\s*=)/,
                inside: {
                    'operator': /(^`+|`+$)/,
                    'attr-name attribute-key':  /.*/
                }
            },
            _escaped_key: {
                pattern: /^(`+)(?!`)(?:(?!\1).)*\1(?=\s*:|\s*$)/,
                inside: {
                    'operator': /(^`+|`+$)/,
                    'tag name': /.*/
                }
            },
            rest: elementRest
        }
    },
    // - ...
    _item: {
        pattern: /(^\s*)-(?!-)[^\n]*$/m,
        lookbehind: true,
        inside: {
            'operator': /^-/,
            _value: /.*/
        }
    },
    // ...
    _element: {
        pattern: /(^\s*)[^\s:=>\-#`][^\n]*$/m,
        lookbehind: true,
        inside: {
            'attr-name attribute-key ': /^[^\s:=>\-#`][^\n:=]*(?==)/,
            'tag name': /^[^\s:=>\-#`][^\n:=]*(?=:|$)/,
            rest: elementRest
        }
    },
    'invalid': /\S+/
};
