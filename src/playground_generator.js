import fs from 'fs';
import path from 'path';

import { attrEscape } from './util.js';

export function playground(data) {
    const html = `
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Eno Playground">
        <link href="../styles.css?${data.assetHash}" rel="stylesheet">
        <link href="../playground.css?${data.assetHash}" rel="stylesheet">
        <script defer src="bundle.js?${data.assetHash}"></script>
        <title>Eno · Playground</title>
    </head>
    <body>
        <div class="site">
            <div class="header">
                <div>
                    <select class="demo">
                        <option disabled selected>Browse Demos</option>
                        <option disabled>──────────</option>
                        ${data.playground.map(group => `
                            <optgroup label="${group.title}">
                                ${group.examples.map(example => `
                                    <option data-eno="${attrEscape(example.eno)}">
                                        ${example.title}
                                    </option>
                                `).join('')}
                            </optgroup>
                        `).join('')}
                    </select>
                </div>

                <h1>Eno <span class="light">· Playground</span></h1>
            </div>

            <div class="split">
                <div class="editor">
                    <textarea id="editor">${data.playground[0].examples[0].eno}</textarea>
                </div>

                <div class="inspector"></div>

                <div class="error"></div>
            </div>
        </div>
    </body>
</html>
    `.trim();

    const dirAbsolute = path.resolve('public/playground');
    fs.mkdirSync(dirAbsolute, { recursive: true });
    fs.writeFileSync(path.join(dirAbsolute, 'index.html'), html);
};
